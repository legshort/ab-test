
// JavaScript Document
function getCookie(name){
	var search = name + "=";

	if (document.cookie.length > 0){
	    offset = document.cookie.indexOf(search);

	    if (offset != -1){
	        offset += search.length;
	        end = document.cookie.indexOf(";",offset);

	        if (end == -1) end = document.cookie.length;
	        return unescape(document.cookie.substring(offset,end));
	    }
	    else return false;
	}
	else return false;
}

function goCompanyIntro(){
	window.open('http://www.skplanet.co.kr/');
	return;
}

function goCopyright(){
	top.location.href = "http://www.11st.co.kr/trsprogram/IprPropertyRightsGuide.tmall";
	return;
}

var color = getCookie("BROWSING_MAIN_COLOR");

var strFooter = "";
var bShowFooterSearch = "Y";
var searchTabNm = "통합검색";
var searchActionUrl = "http://search.11st.co.kr/SearchPrdAction.tmall";
var searchMethodUrl = "getFooterSearchSeller";
var searchTargetTab = "T";
var areaCode = "SRC02";
var noSearchResult = (noSearchResult==undefined) ? false : true;
var footerKwd = (footerKwd==undefined) ? "" : footerKwd;

if ( typeof(bFooterSearch) == 'undefined' )
{
	bFooterSearch = 'Y';
}

if (bFooterSearch == "N"){
	bShowFooterSearch = "N";
}else if (bFooterSearch == "SEMANTIC"){ //시맨틱 검색
	searchTabNm = "시맨틱검색";
	searchActionUrl = "http://search.11st.co.kr/SemanticSearchAction.tmall";
	searchMethodUrl = "getSemanticSearch";
	searchTargetTab = "SEMANTIC";
	areaCode = "SRC02";
}else if (bFooterSearch == "CONTENTS"){ //컨텐츠검색
	searchTabNm = "컨텐츠검색";
	searchActionUrl = "http://search.11st.co.kr/ContentsSearchAction.tmall";
	searchMethodUrl = "getContentsSearch";
	searchTargetTab = "CONTENTS";
	areaCode = "SRC02";
}else if (bFooterSearch == "MODEL"){ //모델별검색
	searchTabNm = "모델별검색";
	searchActionUrl = "http://search.11st.co.kr/ModelSearchAction.tmall";
	searchMethodUrl = "getModelSearch";
	searchTargetTab = "MODEL";
	areaCode = "SRC02";
}else if (bFooterSearch == "BRAND"){ //브랜드 검색
	searchTabNm = "브랜드검색";
	searchActionUrl = "http://search.11st.co.kr/BrandSearchAction.tmall";
	searchMethodUrl = "getBrandSearch";
	searchTargetTab = "BRAND";
	areaCode = "SRC02";
}else if (bFooterSearch == "BT"){ //뷰티11번가
	searchTabNm = "뷰티11번가";
	searchActionUrl = "http://beauty.11st.co.kr/newBeauty/newBeautyAction.tmall";
	searchMethodUrl = "newBeautySearch";
	searchTargetTab = "BT";
	areaCode = "SBB0301";
	footerKwd = fncRemoveScriptTag(document.GNBSearchForm.kwd.value);
}else if (bFooterSearch == "SIC"){ //싸이닉
	searchTabNm = "싸이닉";
	searchActionUrl = "http://scinic.11st.co.kr/beauty/BeautySearchAction.tmall";
	searchMethodUrl = "getBeautyTotalSearch";
	searchTargetTab = "SIC";
	areaCode = "SRC02";
	footerKwd = document.GNBSearchForm.kwd.value;
}else if (bFooterSearch == 'MT11'){ //마트11번가 (검색제거, div class 명 변경)
	bShowFooterSearch = "MT11";
}


var strFSrch = '';
if (bShowFooterSearch == 'Y') {
	strFSrch += '<form name="FooterSearchForm" action="' + searchActionUrl + '">';
	strFSrch += '<input type="hidden" name="method" value="' + searchMethodUrl + '">';
	strFSrch += '<input type="hidden" name="targetTab" value="' + searchTargetTab + '">';
	strFSrch += '<input type="hidden" name="isGnb" value="Y">';
	strFSrch += '<input type="hidden" name="category">';
	strFSrch += '<input type="hidden" name="cmd">';
	strFSrch += '<input type="hidden" name="pageSize">';

	strFSrch += '<div class="footer_search">';
	strFSrch += '<fieldset class="total_search">';
		strFSrch += '<legend><label for="bottomKwd">하단 통합 검색</label></legend>';
		strFSrch += '<h4>' + searchTabNm + '</h4>';
		strFSrch += '<input name="kwd" type="text" class="text" value="" id="bottomKwd"';

		try {
			strFSrch += footerKwd;
		} catch (ex) {
		}

		strFSrch += '" onKeyPress="if(event.keyCode == 13) {goFooterSearch(\''+ areaCode +'\'); return false;}" style="ime-mode:active;">';
		strFSrch += '<input type="button" value="검색" onclick="javascript:goFooterSearch(\''+ areaCode +'\');" class="search">';
	strFSrch += '</fieldset>';

	if(footerKwd != '' && !noSearchResult && bFooterSearch != 'BT' && bFooterSearch != 'SIC') {
		strFSrch += '<a class="btn_research" onclick="searchResearchPop()"><span class="ico_rsch"><em>설문아이콘</em></span>검색결과 설문조사</a>';
	}

	strFSrch += '</div>';
	strFSrch += '</form>';

	try {
		if($ID("footerSearch")) {
			$ID("footerSearch").innerHTML = strFSrch;
		}else{
			strFooter += strFSrch;
		}
	} catch(e) {}
}
strFooter += '<div id="myHistoryArea" style="display:none"></div>';
//페이지 하단 신규 광고
strFooter += '<div id="newFooterAdArea" class="ftr_banner_wrap" style="display:none"></div>';

//오른쪽 날개 배너(HTML) 호출 부분
var IS_NEW_RWB = true;
try{
	if(isWingBnnr && typeof (FooterComm.WingRBnnrHtml) != "undefined" && IS_NEW_RWB){
		var wingR = new FooterComm.WingRBnnrHtml();
		wingR.init();
	}
}catch(e){

}

document.write(strFooter);
document.write(makeFooter(true));
document.write('</div>');	// end wrapBody

var footerPopup = "";
function popupFromFooter(url, popupName){

	if(footerPopup == undefined || footerPopup == ""){
		footerPopup = window.open(url, popupName);
	}else{
		footerPopup = window.open(url, popupName);
		footerPopup.focus();
	}
}

// ============================= 코딩팀시작
// 열리고 닫힘 높이
//080109_2 추가
function	topLink()	{
	if(document.body.scrollTop)
		document.body.scrollTop = 0;
	else
		document.documentElement.scrollTop = 0;
}
//__080109_2 추가

// 0103 수정
function familyLink()	{
	obj_FM = document.getElementById("familyLink_sub");
	if(obj_FM.style.display=="none") {
		jQuery("#footerWrap").css("z-index",100);
		obj_FM.style.display="block";
	}
	else	{
		jQuery("#footerWrap").css("z-index","");
		obj_FM.style.display="none";
	}
}
//_ 0103 수정
var divAccount = document.getElementById("layAccount");
function setY(open,classV){

	if(!classV) classV="acHeight1";
	if (classV=="acHeight1")		H=27;
	else							H=145;

	divAccount.style.height=H + "px";
}
// ============================= 코딩팀시작
/** xtractor_cookie.js Start *****/
function makeXTVIDCookie() {
    if (! isXTVID()) {
        setXTVIDCookie();
	}
}

function isXTVID() {
	var vid = getXTCookie("XTVID");
	if(vid != null && vid != "") {
		return true;
	}
	return false;
}

function getXTCookie(name) {
    var cookies = document.cookie.split("; ");
    for (var i=0; i < cookies.length; i++)  {
        var cPos = cookies[i].indexOf( "=" );
        var cName = cookies[i].substring( 0, cPos );
        if ( cName == name ) {
            return unescape( cookies[i].substring( cPos + 1 ) );
        }
    }
    return "";
}

function setXTVIDCookie(){
    var randomid = Math.floor(Math.random()* 1000);
    var xtvid = "A" + makeXTVIDValue() + randomid;
	expireDate = new Date();
	expireDate.setYear(expireDate.getYear()+ 10);

	setXTCookie("XTVID", xtvid, 365*10, "/", getXDomain());
}

function setXTLIDCookie(userNo){
    setXTCookie("XTLID", userNo, -1, "/", getXDomain());
}

function removeXTCookie(name){
    setXTCookie(name, "", 0, "/", getXDomain());
}

function setXTCookie(name, value, expires, path, domain){
    var todayDate = new Date();
    todayDate.setDate(todayDate.getDate() + expires);
    var expiresInfo = (expires < 0)? '' : todayDate.toGMTString();
    document.cookie = name + "=" +escape(value) + ";" + "path=" + path + ";domain=" + domain + ";expires="+ expiresInfo;
}

function getXDomain() {
	var host = document.domain;
	var tokens = host.split('.');
	var xdomain = tokens[tokens.length-2] + '.' + tokens[tokens.length-1];
	return (tokens[tokens.length-1].length == 2) ? tokens[tokens.length-3] + '.' + xdomain : xdomain;
}

function makeXTVIDValue() {
    var str = '';
    nowdate = new Date();
    digit = nowdate.getYear();
    if (digit < 2000) {
		digit = digit - 1900;
    } else {
		digit = digit - 2000;
	}
	str += paddingNo(digit);

    digit = nowdate.getMonth() + 1;
	str += paddingNo(digit);

    digit = nowdate.getDate();
	str += paddingNo(digit);

    digit = nowdate.getHours();
	str += paddingNo(digit);

	digit = nowdate.getMinutes();
	str += paddingNo(digit);

    digit = nowdate.getSeconds();
	str += paddingNo(digit);

    digit = nowdate.getMilliseconds();
	if ((digit <= 99) && (digit > 9)) {
        str += '0' + digit;
    } else if (digit <= 9) {
        str += '00' + digit;
    } else {
		str += '' + digit;
	}
    return str;
}

function paddingNo(val) {
	var st = '';
	if (val <= 9) {
		st += '0' + val;
	} else {
		st = '' + val;
	}
	return st;
}

//makeXTVIDCookie();
/** xtractor_cookie.js End *****/

if (window.attachEvent) {					// IE
  window.attachEvent("onload", checkImageStopFooter);
} else if (window.addEventListener) {		// FF
  window.addEventListener("load", checkImageStopFooter, false);
}

//2008-12-23 GNB GIF 애니메이션 이미지 정지 설정/해제를 위한 추가
function checkImageStopFooter()	{
	try {
		var checkImage = getCookie("TMALL_MY_IMGSTOP");
		if(checkImage != "") checkImageStop(1);
	} catch(e) {
	}
}

try  {
	var t_prd_list = "";
	var t_cells = document.getElementsByTagName("a");
	var prdNoReg = new RegExp("prdNo=[0-9]*","g");
	var t_prePrdNo = ""
	for (var i = 0; i < t_cells.length; i++) {
		var t_link = t_cells[i].getAttribute("href");
		if ( t_link == null ) continue;
		var t_prdNos = t_link.match( prdNoReg );
		var t_prdNo = ""
		if ( t_prdNos != null && t_prdNos.length > 0 ) {
			if ( t_link.indexOf("SellerProductDetail.tmall") > 0 && t_prdNos[0].length > 6 ) {
				t_prdNo = t_prdNos[0].substring( 6 );
				if ( t_prePrdNo != t_prdNo ) {
					t_prd_list = t_prd_list + t_prdNo +"^";
					t_prePrdNo = t_prdNo;
				}
			}
		}
	}
} catch (t_ex) {}

/* 2009년 11월 02일 02.tMallFrontProject/webapps/js/apengine/makePCookie.js 병합 시작 */
//Content 	: 도메인을 확인하여 유효한 영구쿠키를 발급
//Date 	: 2005.12.07

function Nethru_getCookieVal(offset)
{
	var endstr = document.cookie.indexOf (";", offset);
	if (endstr == -1)
		endstr = document.cookie.length;
	return unescape(document.cookie.substring(offset, endstr));
}

function Nethru_SetCookie(name, value)
{
	var argv = Nethru_SetCookie.arguments;
	var argc = Nethru_SetCookie.arguments.length;
	var expires = (2 < argc) ? argv[2] : null;
	var path = (3 < argc) ? argv[3] : null;
	var domain = (4 < argc) ? argv[4] : null;
	var secure = (5 < argc) ? argv[5] : false;

	document.cookie = name + "=" + escape (value)
	                                + ((expires == null) ? "" : ("; expires="+expires.toGMTString()))
	                                + ((path == null) ? "" : ("; path=" + path))
	                                + ((domain == null) ? "" : ("; domain=" + domain))
	                                + ((secure == true) ? "; secure" : "");
}

function Nethru_GetCookie(name)
{
	var arg = name + "=";
	var alen = arg.length;
	var clen = document.cookie.length;
	var i = 0;

	while (i < clen) {
 		var j = i + alen;
 		if (document.cookie.substring(i, j) == arg)
    			return Nethru_getCookieVal (j);

 		i = document.cookie.indexOf(" ", i) + 1;
 		if (i == 0)
    			break;
 	}

	return null;
}

function Nethru_makePersistentCookie(name,length,path,domain)
{
	var today = new Date();
	var expiredDate = new Date(2020,1,1);
	var cookie;
	var value;

	cookie = Nethru_GetCookie(name);
	if ( cookie ) {		//이미 존재하는지 확인
   		return 1;
	}

	var values = new Array();
	for ( i=0; i < length ; i++ ) {
		values[i] = "" + Math.random();
	}

	value = today.getTime();

	// use first decimal
	for ( i=0; i < length ; i++ ) {
		value += values[i].charAt(2);
	}

	Nethru_SetCookie(name,value,expiredDate,path,domain);
}

function Nethru_getDomain() {
	var _host   = document.domain;
	var so = _host.split('.');
	var dm  = so[so.length-2] + '.' + so[so.length-1];

	return (so[so.length-1].length == 2) ? so[so.length-3] + '.' + dm : dm;
}

var Nethru_domain  = Nethru_getDomain();

Nethru_makePersistentCookie("PCID",10,"/",Nethru_domain);

// 특수문자 치환
function fncRemoveScriptTag(str){
	var specialChars='<>()';
	var result=str;
	var i, j;

	if(result != null){
		if(result.indexOf("script") >= 0 || result.indexOf("iframe") >= 0 || result.indexOf("alert") >= 0)
		for (i = 0; i < result.length; i++) {
			for (j = 0; j < specialChars.length; j++) {
				if (result.charAt(i) == specialChars.charAt(j))
					result = result.replace(result.charAt(i), " ");
			}
		}
	}

	return result;
}

//검색결과 설문조사 팝업
function searchResearchPop(kwd, targetTab) {
	if(kwd == undefined || kwd == '') kwd = GNBSearchForm.kwd.value;
	if(targetTab == undefined || targetTab == '') targetTab = GNBSearchForm.targetTab.value;

	var url = "http://search.11st.co.kr/jsp/search/include/searchSurveyPop.jsp?kwd=" + kwd + "&targetTab=" + targetTab;
	window.open(url, 'searchResearchPop', 'width=430,height=430,scrollbars=no,status=no,resizable=no');
}
/* 2009년 11월 02일 02.tMallFrontProject/webapps/js/apengine/makePCookie.js 병합 끝 */


//  PCID Log
/*
jQuery(document).ready(
	function()
	{
		try
		{
			var pcidActDt = TMCookieUtil.getSubCookie(2, "PCID_ACT_DT");
			if ( pcidActDt.length == 10  )
			{
				var nowDt = new Date();
				var cookie = new Date(
									pcidActDt.substring(0,4)
									, Number(pcidActDt.substring(4,6))-1
									, Number(pcidActDt.substring(6,8))
									, Number(pcidActDt.substring(8,10))
								);
				var timeDiff = (nowDt.getTime() - cookie.getTime()) / 1000 / 60 / 60;

				if ( timeDiff > 12 )
				{
					pcidActDt = "";
				}
			}
			else
			{
				pcidActDt = '';
			}

			if ( pcidActDt == '' )
			{
				var ajaxUrl = "http://www.11st.co.kr/commons/FooterAjaxAction.tmall?method=insertPcidActLog";
				jQuery.ajax(
					{
						url: ajaxUrl,
						dataType: "jsonp",
						success: function(data){}
					}
				);
			}
		} catch(e){}
	}
);
*/
